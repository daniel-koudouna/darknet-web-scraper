var MongoClient = require('mongodb').MongoClient;
// Connection URL
var url = 'mongodb://localhost:27017/darkweb';

if (process.argv.length < 4) {
  console.log("Not enough args :(");
  return;
}

// Use connect method to connect to the server
MongoClient.connect(url, function(err, db) {
  var collection = db.collection(process.argv[2]);

  collection.insertMany(JSON.parse(process.argv[3]), function(err, res) {
    console.log("Successfully added data to the database!");
  });

  db.close();
});
