// URL variables
var VISITED_URLS = [], PENDING_URLS = [];

var MAX_SITES = 1000;
var site_count = 0;

var Method = {
	TORSHOP : 0,
	ALPHABAY : 1
}

function UrlObject(url, status, method) {
		this.url = url;
		this.status = status;
		this.method = method;
}

var ALPHABAY_HOME = "http://pwoah7foa6au2pul.onion";
var ALPHABAY_URL = "http://pwoah7foa6au2pul.onion/login.php";
var ALPHABAY_USERNAME = "linux2016";
var ALPHABAY_PASSWORD = "iloveonions";

// Create instances
var casper = require('casper').create({
	 "viewportSize" : { width: 1600, height: 900},
	 "pageSettings": {
        webSecurityEnabled: false
    }
 });

casper.on('error', function(msg,backtrace) {
   casper.capture('../results/error.png');
	 casper.echo(backtrace, "ERROR");
	 casper.echo(msg, "ERROR");
   throw new ErrorFunc("fatal","error","filename",backtrace,msg);
 });

 var colorizer = require('colorizer').create('Colorizer');

var $ = require('jquery');
var utils = require('utils');
var _ = require('underscore');
var fs = require('fs');
var helpers = require('./helpers');
var moment = require("moment");

var date = moment(new Date().getTime()).format("YYYY-MM-DD-HH:mm")
var filename = "../results/" + date + "/" + date + ".txt";

var websites = require("websites.json");
var keywords = require("keywords.json");
var BLACKLIST = require("blacklist.json");

var START_URL = websites.shift();

casper.waitForFileExec = function(process, args, callback, onTimeout, timeout){
    this.then(function(){
        var cp = require('child_process'),
            finished = false,
            self = this;
				if (!timeout) {
					timeout = this.options.stepTimeout;
				}
        cp.execFile(process, args, {}, function(error, stdout, stderr) {
            finished = true;
            callback.call(self, error, stdout, stderr);
        });
        this.waitFor(function check(){
            return finished;
        }, null, onTimeout, timeout);
    });
    return this; // for builder/promise pattern
};


function getSiteData(urlObject) {
	switch (urlObject.method) {
		case Method.TORSHOP:

			var descriptions = casper.evaluate(function() {
				var descriptions = [];
				$(".info-container").each(function(i,el) {
					var object = {};
					object.name = $(el).find(".product-name").text();
					object.price = $(el).find(".price").text();
					descriptions.push(object);
				});

				$("tr").each(function(i,el) {
					var found = false;
					var object = {};
					$(el).find("td").each(function(i,e) {

							if ($(e).text() != null) {
								var has_price = $(e).text().indexOf("USD") != -1 ||
								$(e).text().indexOf("฿") != -1 ||
								$(e).text().indexOf("BTC") != -1 ||
								$(e).text().indexOf("GBP") != -1;

								if (found && has_price) {
									object.price = $(e).text();
								} else if (!found) {
									found = true;
									object.name = $(e).text();
								}
							}
					});

					if (object.name && object.price) {
						descriptions.push(object);
					}
				});

				return descriptions;
			});

			try {
				if (descriptions != null && descriptions.length > 0) {
					descriptions = _.map(descriptions, function(el) {
						el.scrape_date = date;
						return el;
					});
					// fs.write(filename,JSON.stringify(data) + "\n",'a');
					var args = ["server.js", "topshop_data", JSON.stringify(descriptions)];
					casper.waitForFileExec("node", args, function(err, stdout, stderr) {

					});

				}
			} catch (e) {

			}

			break;
		case Method.ALPHABAY:

			var data = casper.evaluate(function() {

					var data = [];
					$(".listing").each(function(i, el) {
							var listing = {};
							listing.title = $(el).find(".bstd").text();

							$(el).find(".std").each(function() {
									if  ($(this).text().indexOf("#") > 0) {
										listing.category = $(this).text();
									}
									if ($(this).attr('href') != undefined) {
										listing.seller = $(this).text();
									}
									if ($(this).text().indexOf("Views:") >= 0) {
										listing.status = $(this).text();
									}
									if ($(this).text().indexOf("Buy price") >= 0) {
										listing.price = $(this).text();
									}
							});
							if (!(listing.price == "" || listing.price == undefined || listing.title == "" || listing.title == undefined)) {
								data.push(listing);
							}
					});

					return data;
			});



			var feedback = casper.evaluate(function() {
				var feedback = [];

				var url = window.location.href;
				if (url.indexOf("tab=3") != -1) {

					$("#div_content3 .slisting").each(function(i, el) {

								var fb = {};
								var img = $(el).find("img").attr('src');

								if (img && img.indexOf("images/feedback1.png") >= 0) {
									fb.status = "positive";
								} else if (img && img.indexOf("images/feedback3.png") >= 0){
									fb.status = "negative";
								} else {
									fb.status = "neutral";
								}
								fb.buyer = $(el).find(".tcl:nth-child(2) > .std").text();
								fb.date = $(el).find(".tcl:nth-child(3) > .std").text();
								fb.time = $(el).find(".tcl:nth-child(4) > .std").text();
								fb.comment = $(el).find("div:nth-child(5) > .std").text();
								var m = url.match(/id=(\d+)/);
								if (m && m.length == 2) {
									fb.item_id = m[1];
								}

								feedback.push(fb);

					});

				}

				return feedback;
			});




			try {
				if (data != null && data.length > 0) {
					data = _.map(data, function(el) {
						el.scrape_date = date;
						return el;
					});
					// fs.write(filename,JSON.stringify(data) + "\n",'a');
					var args = ["server.js", "data", JSON.stringify(data)];
					casper.waitForFileExec("node", args, function(err, stdout, stderr) {
						casper.echo("Added " + colorizer.format(data.length, {fg: "green"}) + " products");
					});

				}
				if (feedback != null && feedback.length > 0) {
					feedback = _.map(feedback, function(el) {
						el.scrape_date = date;
						return el;
					});

					// fs.write(filename,JSON.stringify(feedback) + "\n",'a');
					var args = ["server.js" , "feedback", JSON.stringify(feedback)];
					casper.waitForFileExec("node", args, function(err, stdout, stderr) {
						casper.echo("Added " + colorizer.format(feedback.length, {fg: "green"}) + " feedback");
					});
				}
			} catch (e) {
				casper.echo(e,"ERROR");
			}

			break;
		default:

	}
}

// Spider from the given URL
function spider(urlObject) {

	// Open the URL
	casper.open(urlObject.url).then(function() {
		casper.wait(5000);

		if (containsCaptcha()) {
			casper.then(function() {
				solveCaptcha(urlObject);
			});
		}
		casper.then(function() {

					// Set the status style based on server status code
					var status = this.status().currentHTTPStatus;
					switch(status) {
						case 200: var statusStyle = { fg: 'green', bold: true }; break;
						case 404: var statusStyle = { fg: 'red', bold: true }; break;
						 default: var statusStyle = { fg: 'magenta', bold: true }; break;
					}

					if (status != 200) {
						casper.echo("Got status " + colorizer.format(status,statusStyle) + ", skipping");
						scrapeNext();
					} else {

						// Display the spidered URL and status
						casper.echo(colorizer.format(status,statusStyle) + ' ' + urlObject.url + colorizer.format(" (" + site_count + "/" + MAX_SITES + ")", {fg: "yellow"}) );

						try {
							fs.write(filename,urlObject.url + "\n",'a');
						} catch (e) {
							casper.echo(e, "ERROR");
						}

						this.capture("../results/" + date + "/" + urlObject.url +  date + "-screenshot.png");

						this.page.injectJs('node_modules/jquery/dist/jquery.js');

						// Find links present on this page
						var links = this.evaluate(function() {
							var links = [];
							Array.prototype.forEach.call(__utils__.findAll('a'), function(e) {
								links.push(e.getAttribute('href'));
							});
							return links;
						});


						getSiteData(urlObject);

						// Add the URL to the visited stack
						urlObject.status = status;
						VISITED_URLS.push(urlObject);

						// Add newly found URLs to the stack
						var baseUrl = this.getGlobal('location').origin;
						Array.prototype.forEach.call(links, function(link) {
							var newUrl = helpers.absoluteUri(baseUrl, link);
							var isNotPending = (_.find(PENDING_URLS, function(obj) {
								return obj.url == newUrl;
							}) == null);
							var isNotVisited = (_.find(VISITED_URLS, function(obj) {
								return obj.url == newUrl;
							}) == null);
							var isInDomain = newUrl.indexOf(START_URL.url) != -1;
							var isNotInBlacklist = (_.find(BLACKLIST, function(obj) {
								return newUrl.indexOf(obj) != -1;
							}) == null);

							var isKeyword = (_.find(keywords, function(kw) {
									return newUrl.indexOf(kw) != -1;
							}) != undefined);

							if (isNotPending && isNotVisited && isInDomain && isNotInBlacklist) {
								var obj = new UrlObject(newUrl,0,urlObject.method);
								if (isKeyword) {
									PENDING_URLS.unshift(obj);
								} else {
									PENDING_URLS.push(obj);
								}
							}
						});

						scrapeNext();
					}

			});


	});

}

function scrapeNext() {
	site_count++;
	if (site_count < MAX_SITES) {

		// If there are URLs to be processed
		if (PENDING_URLS.length > 0) {
			var nextObject = PENDING_URLS.shift();
			casper.echo("spidering next: " + nextObject.url);
			casper.then(function() {
				spider(nextObject);
			});
		} else if (websites.length > 0){
			var nextObject = websites.shift();

			VISITED_URLS = []
			PENDING_URLS = [];

			START_URL = nextObject;
			casper.echo("Finished scraping current site, moving on to " + START_URL.url);
			casper.then(function() {
				spider(nextObject);
			});
		}

	}
}

function containsCaptcha() {
	return (casper.exists("input[name='answer']") || casper.exists("input[name='captcha_code']"));
}

function solveCaptcha(urlObject) {
			casper.echo("Encountered captcha", "COMMENT");

			if (casper.exists("input[name='user']")) {
				casper.sendKeys("input[name='user']", ALPHABAY_USERNAME);
			}
			if (casper.exists("input[name='pass']")) {
				casper.sendKeys("input[name='pass']", ALPHABAY_PASSWORD);
			}

			var width = 250;
			var height = 50;


			var captcha_image = "../results/" + date + "/" + urlObject.url +  date + "-captcha.png";
			var args = ["captcha/solver.py", captcha_image];

			casper.captureSelector(captcha_image, '#captcha , img[alt="Captcha"]');

			casper.capture("../results/" + date + "/" + urlObject.url +  date + "-solving.png");

			return casper.waitForFileExec('python', args, function(error, stdout, stderr) {

				var answer = stdout.replace(/(\r\n|\n|\r)/gm,"");
				casper.echo("Recieved answer for captcha: " + colorizer.format(answer, {fg: "magenta"}));

				if (casper.exists("input[name='captcha_code']")) {
					casper.sendKeys("input[name='captcha_code']", answer);
				}
				if (casper.exists("input[name='answer']")) {
					casper.sendKeys("input[name='answer']", answer);
				}

				casper.capture("../results/" + date + "/" + urlObject.url +  date + "-solving.png");

					casper.click("input[type='submit']");
					casper.then(function() {
						casper.capture("../results/" + date + "/" + urlObject.url +  date + "-solved.png");

						var newObject = new UrlObject(casper.getCurrentUrl(),0,urlObject.method);
						if (containsCaptcha()) {
							solveCaptcha(newObject);
						} else {
							casper.echo("Finished solving captcha", "INFO");
						}
					});

			},function() {

			},900000);
}

// Start spidering
casper.start("www.google.com", function() {
	casper.echo("Casper initialized successfully, starting on " + colorizer.format(START_URL.url, {fg: "yellow"}));
	spider(START_URL);
});

// Start the run
casper.run(function() {

});


casper.then(function() {
	this.exit();
});
